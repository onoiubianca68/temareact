import "./UserCard.css";

function UserCard({ id, username, email, photo, handleUpdate }) {
    return (
        <div className="user-card">
            <img src={photo} alt="Avatar" className="user-card-img" />
            <h3>{username}</h3>
            <p>{email}</p>
            <div className="user-card-buttons">
                <button className="user-card-delete-btn">Delete</button>
                <button
                    className="user-card-update-btn"
                    onClick={() => handleUpdate(id)}
                >
                    Update
                </button>
            </div>
        </div>
    );
}

export default UserCard;
