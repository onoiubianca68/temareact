import { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Register from "./pages/Register/Register";
import Home from "./pages/Home/Home";

function App() {
    const [user, setUser] = useState(false);

    return (
        <div className="app">
            <Router>
                <Routes>
                    {!user ? (
                        <Route path="/" element={<Home />} />
                    ) : (
                        <Route
                            path="/"
                            element={<Register setUser={setUser} />}
                        />
                    )}
                </Routes>
            </Router>
        </div>
    );
}

export default App;
