import { useEffect, useRef, useState } from "react";
import "./Home.css";
import UserCard from "../../components/UserCard/UserCard";
import FormInput from "../../components/FormInput/FormInput";
import { addUserInputs } from "../../data/inputsData";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Home() {
    const [userList, setUserList] = useState([]);
    const [values, setValues] = useState({
        username: "",
        email: ""
    });
    const [isUpdatingMode, setIsUpdatingMode] = useState(false);
    const [updatingId, setUpdatingId] = useState(null);
    const addFormRef = useRef(null);

    useEffect(() => {
        const getUsers = () => {
            fetch("https://jsonplaceholder.typicode.com/users")
                .then(res => res.json())
                .then(json => {
                    const firstUsers = json.slice(0, 5);
                    const usersWithPhotos = firstUsers.map(user => {
                        return {
                            ...user,
                            photo: `https://picsum.photos/${Math.round(
                                100 + Math.random() * 10
                            )}`
                        };
                    });
                    setUserList(usersWithPhotos);
                });
        };
        getUsers();
    }, []);

    const getMaxId = userList => {
        let maxId = 0;

        userList.forEach(user => {
            if (user.id > maxId) {
                maxId = user.id;
            }
        });

        return maxId;
    };

    const handleSubmit = e => {
        e.preventDefault();

        if (!values.username.match("^[A-Za-z0-9]{3,16}$")) {
            toast.error(
                "Username should be 3-16 characters and shouldn't include any special character!"
            );
            return;
        }

        if (isUpdatingMode === true) {
            setUserList(prevUserList => {
                const updatedUsers = prevUserList.map(user =>
                    user.id === updatingId
                        ? {
                              id: updatingId,
                              ...user,
                              ...values
                          }
                        : user
                );
                return updatedUsers;
            });
        } else {
            setUserList(prevUserList => {
                return [
                    {
                        id: getMaxId(prevUserList) + 1,
                        photo: `https://picsum.photos/${Math.round(
                            200 + Math.random() * 100
                        )}`,
                        ...values
                    },
                    ...prevUserList
                ];
            });
        }
        setValues({
            username: "",
            email: ""
        });
        setIsUpdatingMode(false);
    };

    const handleChange = e => {
        setValues(prevValues => {
            return { ...prevValues, [e.target.name]: e.target.value };
        });
    };

    const handleUpdate = id => {
        console.log(id);

        const userToUpdate = userList.filter(user => user.id === id);
        const userObj = userToUpdate[0];

        setValues(prevValues => {
            return {
                ...prevValues,
                username: userObj.username,
                email: userObj.email
            };
        });
        addFormRef.current.scrollIntoView();
        setIsUpdatingMode(true);
        setUpdatingId(id);
    };

    return (
        <div className="home">
            <form
                className="add-user-form"
                onSubmit={handleSubmit}
                ref={addFormRef}
            >
                <h1 className="add-user-form-title">Add user</h1>
                {addUserInputs.map(input => (
                    <FormInput
                        key={input.id}
                        {...input}
                        value={values[input.name]}
                        onChange={handleChange}
                    />
                ))}
                <button className="register-form-submit-btn">Add user</button>
                <ToastContainer />
            </form>

            <div className="cards">
                {userList.map(user => {
                    return (
                        <UserCard
                            key={user.id}
                            id={user.id}
                            username={user.username}
                            email={user.email}
                            photo={user.photo}
                            handleUpdate={handleUpdate}
                        />
                    );
                })}
            </div>
        </div>
    );
}

export default Home;
